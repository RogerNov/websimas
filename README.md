Sitio Web del Servicio de información Mesoamericano sobre Agricultura Sostenible

Estructura del sitio
pasando de Almidón a Django
- Noticias
- Eventos
- Publicaciones
    - Tipos de publicaciones
    - Tematicas
    - Disponibilidad
- Multimedia
- Blogs
- Paginas (FlatPages)
- Autores
- Sistemas
- Usuarios
- Imagenes
- Videos
- Audios
    - Categorias
- Tematicas
- Categorias (blogs)
- Aliados
- Servicios
- Paises
- Finaciadores

Versiones a usar en websimas
- PostgreSQL >= 9.2
- Django >= 1.6.2
- Boostrap >= 3.0.3
- JQuery >= 1.10

En todo caso revisar el requirements.txt

@sacrac1 - Carlos A. Rocha Castro