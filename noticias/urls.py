from django.urls import path
from .views import HomePageView, ListNewsView, DetailNewsView, HistoriaPageView, QuienesPageView, ContactenosPageView, ListServiceView, ListPortfolioView

app_name = 'noticias'

urlpatterns =   [
    path('', HomePageView.as_view(), name = "home"),
    path('portafolio/servicios/', ListServiceView.as_view(), name = "servicios"),
    path('portafolio/sistemas/', ListPortfolioView.as_view(), name = "portafolio1"),
    path('historia-simas/', HistoriaPageView.as_view(), name = "historia"),
    path('quienes-somos/', QuienesPageView.as_view(), name = "somos"),
    path('noticias/', ListNewsView.as_view(), name = "noticias"),
   	path('noticias/<int:id>/<str:uri>/', DetailNewsView, name = "detalle-noticia"),
    path('contactenos/', ContactenosPageView.as_view(), name = "contactenos"),
]

