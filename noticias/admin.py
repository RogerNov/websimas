from django.contrib import admin
from .models import Noticia


class NoticiasAdmin(admin.ModelAdmin):
    filter_horizontal = ('idtematica',)
    date_hierarchy = 'fecha'
    list_display = ('id', 'noticia', 'fecha','idautor')
    list_display_links = ('id', 'noticia')
    list_filter = ('noticia','idautor','idtematica')
    search_fields = ('noticia','idautor','idtematica')
    fieldsets = (
        (None, {
            'fields': (('noticia', 'fecha'),'descripcion', ('foto','credito'),'url','uri','texto','tipo',
                       'idtematica',('importante','claves','idautor'))
        }),
        ('Archivos para adjuntar', {
            'fields': (('titarchivo1', 'archivo1'), ('titarchivo2','archivo2'),
                      ('archivo', 'archivo3'))
        }),
    )
# Register your models here.
admin.site.register(Noticia, NoticiasAdmin)
