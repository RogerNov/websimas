from django.contrib import admin
from .models import Blog, Catblog

class BlogAdmin(admin.ModelAdmin):
    list_display = ('blog', 'idautor', 'fecha', 'tags')

class CatblogAdmin(admin.ModelAdmin):
    pass
# Register your models here.
admin.site.register(Blog, BlogAdmin)
admin.site.register(Catblog, CatblogAdmin)
