# -*- coding: utf-8 -*-
from django.db import models
from fotos.models import *
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here.
class Organizaciones(models.Model):
    nombre = models.CharField(max_length=250)
    iniciales = models.CharField(max_length=150)

    def __str__(self):
        return self.iniciales

class Portafolio(models.Model):
    servicio = models.CharField('Nombre del servicio', max_length=250)
    resumen = models.TextField()
    fecha = models.DateField('Fecha de realización')
    link = models.URLField(blank=True)
    organizacion = models.ManyToManyField(Organizaciones)

    fotos = GenericRelation(Fotos)

    def __str__(self):
        return self.servicio

